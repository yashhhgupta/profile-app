import { Component } from '@angular/core';
import { Input,Output } from '@angular/core';
import { Users } from '../types';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [],
  templateUrl: './card.component.html',
  styleUrl: './card.component.scss',
})
export class CardComponent {
  @Input() user!: Users;
  @Input() selectedUser!: Users | null;
  @Output() selectUserEvent = new EventEmitter<Users>();

  selectUser() {
    this.selectUserEvent.emit(this.user);
  }

}
