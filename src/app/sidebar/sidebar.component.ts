import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Users } from '../types';
import { CommonModule } from '@angular/common';
import { CardComponent } from '../card/card.component';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [CommonModule, CardComponent],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss',
})
export class SidebarComponent {
  @Input() users: Users[] = [];
  @Input() selectedUser!: Users | null;
  @Output() selectUserEvent = new EventEmitter<Users>();

  ngOnInit(): void {
    console.log(this.users);
  }

  selectUser(user: Users) {
    this.selectUserEvent.emit(user);
  }


}
