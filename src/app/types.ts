export interface Users {
    id?: number | undefined;
    name: string;
    email: string;
    phone: string;
    website: string;
    age:any;
    role: string; 
}

// users:Users[] = [
//     {
//       id: 1,
//       name: 'John Doe',
//       email: 'john@gmail.com',
//       phone: '1234567890',
//       website: 'johndoe.com',
//       age: 25,
//       role: 'Software Developer',
//     },
//     {
//       id: 2,
//       name: 'Jane Smith',
//       email: 'jane.smith@example.com',
//       phone: '0987654321',
//       website: 'janesmith.io',
//       age: 30,
//       role: 'Data Analyst',
//     },
//     {
//       id: 3,
//       name: 'Michael Johnson',
//       email: 'michael.j@example.com',
//       phone: '5555555555',
//       website: 'michaeljohnson.dev',
//       age: 28,
//       role: 'Project Manager',
//     },
//     {
//       id: 4,
//       name: 'Emily Brown',
//       email: 'emily.b@example.com',
//       phone: '6666666666',
//       website: 'emilybrown.design',
//       age: 32,
//       role: 'Graphic Designer',
//     },
//     {
//       id: 5,
//       name: 'David Wilson',
//       email: 'david.wilson@example.com',
//       phone: '7777777777',
//       website: 'davidwilson.dev',
//       age: 27,
//       role: 'Web Developer',
//     },
//     {
//       id: 6,
//       name: 'Sarah Miller',
//       email: 'sarah.miller@example.com',
//       phone: '9999999999',
//       website: 'sarahmiller.io',
//       age: 29,
//       role: 'UX Designer',
//     },
//     {
//       id: 7,
//       name: 'Chris Anderson',
//       email: 'chris.a@example.com',
//       phone: '4444444444',
//       website: 'chrisanderson.design',
//       age: 31,
//       role: 'Frontend Developer',
//     },
//     {
//       id: 8,
//       name: 'Amanda Thomas',
//       email: 'amanda.t@example.com',
//       phone: '3333333333',
//       website: 'amandathomas.io',
//       age: 26,
//       role: 'UI Designer',
//     },
//     {
//       id: 9,
//       name: 'Matthew Clark',
//       email: 'matthew.c@example.com',
//       phone: '2222222222',
//       website: 'matthewclark.dev',
//       age: 34,
//       role: 'Backend Developer',
//     },
//     {
//       id: 10,
//       name: 'Olivia Martinez',
//       email: 'olivia.m@example.com',
//       phone: '1111111111',
//       website: 'oliviamartinez.io',
//       age: 33,
//       role: 'Full Stack Developer',
//     },
//   ];