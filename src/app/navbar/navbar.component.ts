import { Component, Input,Output,EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Users } from '../types';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
})
export class NavbarComponent {
  @Input() count!: number;
  @Output() setShowFormEvent = new EventEmitter<Users | null>();
  onAdd() {
    this.setShowFormEvent.emit(null);
  }
}
