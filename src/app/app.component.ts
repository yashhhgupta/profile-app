import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BodyComponent } from './body/body.component';
import { Users } from './types';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    SidebarComponent,
    NavbarComponent,
    BodyComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'ProfileApp';
  users: Users[] = [
    {
      id: 1,
      name: 'John Doe',
      email: 'john@gmail.com',
      phone: '1234567890',
      website: 'johndoe.com',
      age: 25,
      role: 'Software Developer',
    },
    {
      id: 2,
      name: 'Jane Smith',
      email: 'jane.smith@example.com',
      phone: '0987654321',
      website: 'janesmith.io',
      age: 30,
      role: 'Data Analyst',
    },
    {
      id: 3,
      name: 'Michael Johnson',
      email: 'michael.j@example.com',
      phone: '5555555555',
      website: 'michaeljohnson.dev',
      age: 28,
      role: 'Project Manager',
    },
    {
      id: 4,
      name: 'Emily Brown',
      email: 'emily.b@example.com',
      phone: '6666666666',
      website: 'emilybrown.design',
      age: 32,
      role: 'Graphic Designer',
    },
    {
      id: 5,
      name: 'David Wilson',
      email: 'david.wilson@example.com',
      phone: '7777777777',
      website: 'davidwilson.dev',
      age: 27,
      role: 'Web Developer',
    },
  ];
  userCount = this.users.length;
  visibleUser: Users | null = null;
  showForm:Boolean = false;
  addUserToArray(user: Users) {
    this.users.push(user);
  }
  deleteUserFromArray(id: number) {
    this.users = this.users.filter((user) => user.id !== id);
  }
  editUserInArray(user: Users) {
    this.users = this.users.map((u) => (u.id === user.id ? user : u));
  }
  setVisibility(user: Users) {
    this.showForm = true;
    this.visibleUser = user;
    console.log('visible22', this.visibleUser);
  }
  removeVisibility() { 
    this.showForm = false;
    this.visibleUser = null;
  }
  setShowForm() { 
    this.showForm = !this.showForm;
  }
}
