import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Users } from '../types';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-body',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './body.component.html',
  styleUrl: './body.component.scss',
})
export class BodyComponent {
  @Input() user!: Users | null;
  @Output() addUserEvent = new EventEmitter<Users>();
  @Output() removeUserEvent = new EventEmitter<number>();
  @Output() editUserEvent = new EventEmitter<Users>();
  @Output() removeVisibilityEvent = new EventEmitter<boolean>();
  name: string = '';
  email: string = '';
  phone: string = '';
  website: string = '';
  role: string = '';
  age: string = '';
  isEditable: boolean = false;
  isDeletable: boolean = false;
  isEditing: boolean = false;

  ngOnChanges(): void {
    this.name = this.user?.name || '';
    this.email = this.user?.email || '';
    this.phone = this.user?.phone || '';
    this.website = this.user?.website || '';
    this.role = this.user?.role || '';
    this.age = this.user?.age.toString() || '';
    if (this.user?.id) {
      this.isEditable = true;
      this.isDeletable = true;
    }
  }
  changeEditStatus() {
    this.isEditing = !this.isEditing;
    console.log(this.isEditing);
  }
  deleteUser() {
    if (!this.user) {
      return;
    }
    this.removeUserEvent.emit(this.user.id);
    this.name = '';
    this.email = '';
    this.phone = '';
    this.website = '';
    this.role = '';
    this.age = '';
    this.isEditable = false;
    this.isDeletable = false;
  }
  addUser() {
    if (
      !this.name ||
      !this.email ||
      !this.phone ||
      !this.website ||
      !this.role ||
      !this.age
    ) {
      return;
    }
    if (!this.user?.id) {
      const id = Math.floor(Math.random() * 100);
      const user: Users = {
        id,
        name: this.name,
        email: this.email,
        phone: this.phone,
        website: this.website,
        role: this.role,
        age: this.age,
      };
      this.addUserEvent.emit(user);
    } else {
      const user: Users = {
        id: this.user.id,
        name: this.name,
        email: this.email,
        phone: this.phone,
        website: this.website,
        role: this.role,
        age: this.age,
      };
      this.editUserEvent.emit(user);
    }
    this.name = '';
    this.email = '';
    this.phone = '';
    this.website = '';
    this.role = '';
    this.age = '';
    this.isDeletable = false;
    this.removeVisibilityEvent.emit();
  }
}
